#!/bin/bash
# rpdzkj build script 
# Platform : Rockchip Android
# Version : v1.0
# Update Date : 20210811


function check_env {
	echo "---------- build environment test-----------"
	
	# MEMORY TOTal greater than 8G
	MEM="$(cat /proc/meminfo | grep MemTotal | tr -cd "[0-9]")"
	if [ $MEM -gt 8000000 ] ; then
		echo "MEMORY is OK"
	else
		echo "-------------------------------------"
		echo "PC memory less than 8G, Please add memory"
		echo "-------------------------------------"
		exit
	fi
	
	# DISK greater than 50G
	DISK="$(df . | awk '{print $4}' | tr -cd "[0-9]")"
	if [ $DISK -gt 50000000 ] ; then
		echo "DISK is OK"
	else
		echo "-------------------------------------"
		echo "PC DISK less than 50G, Please add DISK"
		echo "-------------------------------------"
		exit
	fi

	# SDK PATH Don't put in /mnt/hgfs
	FIRST_DIR=`pwd |  cut -d "/" -f 2`
	SECOND_DIR=`pwd |  cut -d "/" -f 3`
	if [[ $FIRST_DIR = "mnt" ]] && [[ $SECOND_DIR = "hgfs" ]] ; then
		echo "-------------------------------------"
		echo "Don't put SDK in /mnt/hgfs/, Please move to other dir"
		echo "-------------------------------------"
		exit
	fi
}

function build_tar_git {
	if [ ! -d release_dir ];then
		mkdir release_dir
	fi
	
	DATE=`date +%Y%m%d-%H%M%S`
	SDK_NAME=$TARGET_DEVICE-$TARGET_SYSTEM-$DATE
	
	tar -zcvf $SDK_NAME.tar.gz .git
	if [ $? -eq 0 ]; then
		
		# create md5
		md5sum $SDK_NAME.tar.gz > md5.txt
		mv $SDK_NAME.tar.gz release_dir/
		mv md5.txt release_dir/
		
		echo "tar git ok, file create in release_dir"
	else
		echo "package git failed!"
		exit 1
	fi
}

function build_batch {
	if [ ! -d release_dir ];then
		mkdir release_dir
	fi

	# choose board
	build_init

	# set environment
	source $BOARDCONFIG_RP
	lunch $TARGET_LUNCH
	IMAGE_DIR=rockdev/Image-$PRODUCT_NAME/update-*.img

	# get all lcd.dtsi
	i=0
	IFS=$'\n'
	for line in `grep -r "rp-lcd" $DTS_DIR | sed 's/\///g'`
	do
		DTSI_ARRAY1[${i}]=$line
		let i=${i}+1
	done
	unset IFS

	
	# print lcd.dtsi
	echo -n "------------ ready build image num is "
	echo -n ${#DTSI_ARRAY1[@]}
	echo " -------------"
	for ((i=0; i<${#DTSI_ARRAY1[@]}; i++))
	do
		echo ${DTSI_ARRAY1[$i]} #| sed 's/include/& /'
	done
	
	# find null line in dts
	DTS_LINE=`grep -rn "rp-lcd" $DTS_DIR | awk 'END {print}' | cut -d ":" -f 1`
	sed -i "${DTS_LINE}a //nullLine" $DTS_DIR	# add null line after last lcd dtsi
	NULL_LINE=`expr $DTS_LINE + 1`
	STRING=`cat $DTS_DIR | tail -n +$NULL_LINE | head -1`
	if [ -z $STRING ] ; then
		echo -n $TARGET_BUILD_DTB
		echo -n " +"
		echo -n $NULL_LINE
		echo " is NULL"
	fi
	
	# start build
	DTSI_LENGTH=${#DTSI_ARRAY1[*]}
	sed -i '/^#include "rp-lcd/ s/^/\/\//' $DTS_DIR		# commemt lcd dtsi that have included
	
	for((i=0;i<$DTSI_LENGTH;i++));
	do
		echo -e "\n\t====> now, screen board and dtsi is $TARGET_BUILD_DTB:${DTSI_ARRAY1[$i]}\n"
		sed -i -e "${NULL_LINE}s/.*$/${DTSI_ARRAY1[$i]}/" ${DTS_DIR}

		./build.sh                      ###must need, main compile script
		if [ $? -ne 0 ];then
			echo -e "\n\t====> build.sh fail!\n"
			exit -3
		else
			screen=${DTSI_ARRAY1[$i]}
			screen=${screen#*lcd-} && screen=${screen%.*}
			if [[ $screen =~ "ai" ]];then
				screen=${screen#*ai-}
			fi
			
			TIME=`date +%Y%m%d-%H%M%S`
			TARGET_IMAGE_NAME=update-$TARGET_BUILD_DTB-$TARGET_SYSTEM-$screen-$TIME
			
			mv $IMAGE_DIR release_dir/$TARGET_IMAGE_NAME.img
		fi
	
	done
	echo -e "\e[32m finish batch pack\e[0m"
	sed -i -e "${NULL_LINE}d" $DTS_DIR	# pack finished, remove null line
	
}

# build init
function build_init {
	check_env
	echo "==========You're building on Android========="
	echo "Please choose BoardConfig"

	TARGET_BOARD_ARRAY=( $(cd $DEVICE_DIR && ls BoardConfig-*.mk | sort) )
	echo "${TARGET_BOARD_ARRAY[@]}" |xargs -n 1 | sed "=" | sed "N;s/\n/. /"
	read -p "Please input num: " INDEX
	INDEX=$((${INDEX:-0} - 1))
	BUILD_TARGET_BOARD="${TARGET_BOARD_ARRAY[$INDEX]}"
	echo "build target board configuration: $BUILD_TARGET_BOARD"
	if  [ $BUILD_TARGET_BOARD ];then
		cp $DEVICE_DIR/$BUILD_TARGET_BOARD $BOARDCONFIG_RP
		echo -e "switch to $BUILD_TARGET_BOARD\n"
	else
		echo -e "\n\t\e[31m##########incorrect board number##########\e[0m\n"
		exit -1
	fi
}


# build clean
function build_clean {
	cd u-boot && make clean && make mrproper &&  make distclean && cd -
	cd kernel && make clean && cd -
	make installclean
}


# build uboot
function build_uboot {
	echo "start build uboot"
	if [ "$BUILD_UBOOT_WITH_MKV" = true ] ; then
		if [ "$ARCH" == "arm64" ] ; then
			cd u-boot && make $UBOOT_DEFCONFIG && ./mkv8.sh && cd -
		else
			cd u-boot && make $UBOOT_DEFCONFIG && ./mkv7.sh && cd -
		fi
	else
		cd u-boot && ./make.sh $UBOOT_DEFCONFIG && cd -
	fi
	
	if [ $? -eq 0 ]; then
		echo "Build uboot ok!"
	else
		echo "Build uboot failed!"
		exit 1
	fi
}


# build kernel
function build_kernel {
	echo "Start build kernel"

	#cd kernel && make $ADDON_ARGS ARCH=arm64 rockchip_defconfig rk356x_evb.config android-11.config && cd -
	if [[ $TARGET_DEVICE = "rk3566" ]] || [[ $TARGET_DEVICE = "rk3568" ]] ; then
		BOOT_IMG="BOOT_IMG=boot-$TARGET_DEVICE.img"
	fi

	cd kernel && make $BOOT_IMG ARCH=$ARCH $KERNEL_DTS.img -j$BUILD_JOBS && cd -
	if [ $? -eq 0 ]; then
		echo "Build kernel ok!"
	else
		echo "Build kernel failed!"
		exit 1
	fi
	
	if [[ $TARGET_DEVICE = "rk3566" ]] || [[ $TARGET_DEVICE = "rk3568" ]] ; then
		echo "package resoure.img with charger images"
		cd u-boot && ./scripts/pack_resource.sh ../kernel/resource.img && cp resource.img ../kernel/resource.img && cd -
		cp kernel/boot.img $IMAGE_PATH
		cp kernel/boot.img out/target/product/$TARGET_PRODUCT/boot.img
		cp kernel/arch/arm64/boot/Image out/target/product/$TARGET_PRODUCT/kernel
	fi
}

# build android
function build_android {
    echo "start build android"
    make -j$BUILD_JOBS
    # check the result of make
    if [ $? -eq 0 ]; then
        echo "Build android ok!"
    else
        echo "Build android failed!"
        exit 1
    fi
}


function build_mkimage {
    # mkimage.sh
    echo "make and copy android images"
    ./mkimage.sh
    if [ $? -eq 0 ]; then
        echo "Make image ok!"
    else
        echo "Make image failed!"
        exit 1
    fi

}

# build ota
function build_ota {
    INTERNAL_OTA_PACKAGE_OBJ_TARGET=obj/PACKAGING/target_files_intermediates/$TARGET_PRODUCT-target_files-*.zip
    INTERNAL_OTA_PACKAGE_TARGET=$TARGET_PRODUCT-ota-*.zip
    echo "generate ota package"
    ./mkimage.sh ota
    cp $OUT/$INTERNAL_OTA_PACKAGE_TARGET $IMAGE_PATH/
    cp $OUT/$INTERNAL_OTA_PACKAGE_OBJ_TARGET $IMAGE_PATH/
}

# build mkimage
function build_mkupdate {
    mkdir -p $PACK_TOOL_DIR/rockdev/Image/
    cp -f $IMAGE_PATH/* $PACK_TOOL_DIR/rockdev/Image/

    echo "Make update.img"
    cd $PACK_TOOL_DIR/rockdev && ./mkupdate.sh && cd -
    if [ $? -eq 0 ]; then
        echo "Make update image ok!"
    else
        echo "Make update image failed!"
        exit 1
    fi
    
    LCD_NAME=`grep '^#include.*rp.*lcd-[^g][^p]' $DTS_DIR | sed 's/^#include.*-lcd-//g' | sed 's/.dts.*//g'`
    LCD_NAME=${LCD_NAME:-lcd}
    TIME=`date +%Y%m%d-%H%M%S`
    mv $PACK_TOOL_DIR/rockdev/update.img $IMAGE_PATH/update-$TARGET_BUILD_DTB-$TARGET_SYSTEM-$LCD_NAME-$TIME.img -f
    rm $PACK_TOOL_DIR/rockdev/Image -rf
}

# build all
function build_all {
	build_uboot
	build_kernel
	build_android
	build_mkimage
	build_mkupdate
}


export LC_ALL=C
DEVICE_DIR="$(find device/ -name "BoardConfig-*.mk" | xargs dirname | uniq)"
source build/envsetup.sh >/dev/null
BOARDCONFIG_RP=$DEVICE_DIR/.BoardConfig.mk
echo $BOARDCONFIG_RP

if [ -f $BOARDCONFIG_RP ];then
	grep "TARGET_LUNCH" $BOARDCONFIG_RP || (rm $BOARDCONFIG_RP && echo "bad config file, reinitialize...")
fi

if [[ ! -f $BOARDCONFIG_RP ]]; then
	build_init
	if [ "x$@" == "xinit" ];then
		exit 0
	fi
fi

source $BOARDCONFIG_RP
lunch $TARGET_LUNCH


#set jdk version
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib:$JAVA_HOME/lib/tools.jar


# source environment and chose target product
KERNEL_DTS=$TARGET_BUILD_DTB
UBOOT_DEFCONFIG=$UBOOT_DEFCONFIG
KERNEL_DEFCONFIG=$KERNEL_DEFCONFIG
BUILD_JOBS=$(expr `cat /proc/cpuinfo| grep "processor"| wc -l` / 3 \* 2)
TARGET_PRODUCT=$TARGET_PRODUCT
PACK_TOOL_DIR=RKTools/linux/Linux_Pack_Firmware
IMAGE_PATH=rockdev/Image-$TARGET_PRODUCT
TARGET_SYSTEM=$TARGET_SYSTEM
export PROJECT_TOP=`gettop`



OPTIONS="$@"
for option in ${OPTIONS:-all}; do
	echo "processing option: $option"
	case $option in
		BoardConfig*.mk)
            CONF=$TOP_DIR/device/rockchip/$RK_TARGET_PRODUCT/$option
            echo "switching to board: $CONF"
            if [ ! -f $CONF ]; then
                echo "not exist!"
                exit 1
            fi

            ln -sf $CONF $BOARD_CONFIG
            ;;
		*)
			eval build_$option
			;;
	esac
done
