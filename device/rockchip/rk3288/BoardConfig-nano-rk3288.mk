# BoardConfig.mk文件命令格式：BoardConfig-xxx-xxx.mk
# 例如：BoardConfig-pro-rk3288.mk

# ARCH
export ARCH=arm

# DEVICE NAME
export TARGET_DEVICE=rk3288
export TARGET_PRODUCT=rk3288

# PRODUCT NAME
export PRODUCT_NAME=rk3288

# LUNCH
export TARGET_LUNCH=rk3288-userdebug

# SYSTEM
export TARGET_SYSTEM=android8.1

# UBOOT DEFCONFIG
export UBOOT_DEFCONFIG=rk3288_secure_defconfig

# KERNEL DEFCONFIG
export KERNEL_DEFCONFIG=rockchip_defconfig

# DTS
export TARGET_BUILD_DTB=nano-rk3288
export DTS_DIR=kernel/arch/arm/boot/dts/nano-rk3288.dts

# UBOOT BUILD WITH MKV*.SH
export BUILD_UBOOT_WITH_MKV=true
